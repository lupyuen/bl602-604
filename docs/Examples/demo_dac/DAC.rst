.. _gpio-index:

DAC
==================

总览
------

本示例主要介绍如何使用DAC播放音乐

使用步骤
-----------

- 编译 ``customer_app/sdk_app_dac`` 工程并下载工程；



应用实例:
在 main函数中调用audio_dac_dma_test()，开始播放音频。

可以听到 循环播放音乐。



