Developer Environment
========================

此目录介绍编译下载和调试等工具的使用。

编译下载
-----------

用户可以使用BL自主开发的工具 ``BLFlashEnv`` 进行编译和下载，具体使用方法参考 `BLFlashEnv <BLFlashEnv/BLFlashEnv.html>`__ 。

Debug
---------

用户可以在 ``Eclipse + OpenOCD`` 和  ``Freedom Studio + OpenOCD`` 两种方式中选择一种方式进行debug。具体调试方法参考 `Eclipse <eclipse/eclipse.html>`__ 和 `Freedom Studio <freedom_studio/freedom_studio.html>`__ 。

